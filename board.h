#ifndef __BOARD_H__
#define __BOARD_H__

#include <bitset>
#include "common.h"
using namespace std;

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;   
    bitset<64> validMoves;    
 
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    int getValidMoves(Side side);
    Move *getRandomMove(Side side); 
    Move *parseMove(int bit);
    Move *basicHeuristicMove(Side side);
    Move *minimaxMove(Side side, int ply);
    int minimaxGain(Move *m, Board *branch, Side side, Side original, int ply);
    int scoreMove1(Move *m, Side side);
    void setBoard(char data[]);
};

#endif
