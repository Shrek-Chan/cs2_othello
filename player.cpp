#include "player.h"
// hi here's a change

Side playerSide;
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
     this->playerSide = side;
     this->gameBoard = new Board();
     
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
    Side other = (playerSide == BLACK) ? WHITE : BLACK;
    if(opponentsMove != NULL)
    {
	this->gameBoard->doMove(opponentsMove, other);
    }  
    if(this->gameBoard->hasMoves(playerSide))
    {
	/* The minimax code. Currently not configured to work with testmini-
         * max. Follows described heuristics. Does not have corner priority
         * implemented. Now has corner priority implemented as of part 2 of
         * the assignment. This was discarded over greedy + corner though, 
         * since for whatever reason (presumably limited testing statistics
         * due to limitations) pure greedy+corner became stronger than 
         * greedy+corner+minmaxing. That wins around 16/20 rather than 13-14
         * out of 20 here against ConstantTimePlayer.
	 *

	Move* yourMove = this->gameBoard->minimaxMove(playerSide, 2);
	this->gameBoard->doMove(yourMove, playerSide);
	return yourMove;*/

	/* This heuristic is greedy with corner accounting. This is good 
	 * enough to beat the random AI 20/20 times when tested. I will
	 * consider this 'consistently'*/
	Move* yourMove = this->gameBoard->basicHeuristicMove(playerSide);
	this->gameBoard->doMove(yourMove, playerSide);
	return yourMove;
		

	/* This is the random walk code that meets working AI functionality
	   with purely random moves
	Move* yourMove = this->gameBoard->getRandomMove(playerSide);
	this->gameBoard->doMove(yourMove, playerSide);
	return yourMove;
	*/
    }
    else
	return NULL;
}
