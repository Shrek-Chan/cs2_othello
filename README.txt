i like pie
ok then
As the solo group member, I contributed everything to this project myself
over the last two weeks. I spent the last week implementing every last 
thing advised on the set, going through setting up the board, greedy algorithm,
greedy algorithm with corner prioritization, and finally minmaxing. This week
I actually went online and researched an othello strategy guide at 
radagast.se/othello/Help/strategy.html, and attempted to implement a certain
amount of them - for whatever reason, almost certainly the lack of statistics
and small sample size brought about by the lack of an effective testing suite
and me being forced to sit through every game I tested, pure greedy+corners
came out higher than the AI more often than the more complex methods, so
I ended up submitting that.

In the end, I ended up making no improvements to my final submission, though I
attempted many other strategies that didn't work out. I feel that my final
submission, the simply corners + greedy, will work in the tournament entirely
due to the KISS philosophy - by keeping it simple and stupid, it may manage
to beat out a lot of other people in the tournament. As supported by online
research, the emphasis on corners in Othello is huge. While other people
using minimax or learning or databases might have some caveats for avoiding
the corner, my code simply prioritizes corners over everything, meaning I can 
get those and then have a higher chance of beating out everything else, 
especially if other code doesn't have a high enough weight on a corner to beat
out any potential losses from minmaxing into the future. Also, because the code
will behave nothing like any researched high-level strategy, its ability to 
behave illogically will break other strategies.

I tried combining minmaxing with prioritization on moves made into corners; 
the scoring can still be seen commented out in the code. This for all intents
and purposes should have worked, given more tests, but was less effective
with what tests I made. For reasons above I will stick with the simplest
technique. As per my research, I also tried adding to the heuristic to 
prioritize squares next to taken corners. I also attempted to implement
the wedges technique described on that site. 
