#include "board.h"
#include <cstdlib>
#include <stdio.h>
/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);   
    black.set(3 + 8 * 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    newBoard->validMoves = validMoves;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) return true;
        }
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Fills an array with all valid moves this turn. Returns the total number
 * of valid moves. Takes a side
 */
int Board::getValidMoves(Side side) {
   validMoves.reset(); 
   for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move move(i, j);
            if (checkMove(&move, side)) {
		validMoves.set(i + 8*j);
	  }
        }
    }
    return validMoves.count();	
}

/*
 * Extracts a move from a bitset
 */
Move* Board::parseMove(int bit) {
    int i = bit % 8;
    int j = bit / 8;
    Move* move = new Move(i, j);
    return move;
}

/*
 * Tests and scores a move following the first heuristic
 */
int Board::scoreMove1(Move *m, Side side) {
    int score = 0;
    Board *testBoard = copy();
    testBoard->doMove(m, side);
    score = testBoard->count(side);
    int X = m->getX();
    int Y = m->getY();
    //corners and around corners
    if((X == 0 && Y == 0) || (X == 7 && Y == 0) || (X == 0 && Y == 7) ||
		(X == 7 && Y == 7))
	score *= 3;
    else if((X == 1 && Y == 1) || (X == 0 && Y == 1) || (X == 1 && Y == 0)
	|| ( X == 6 && Y == 0) || (X == 7 && Y == 1) || (X == 6 && Y == 1)
	|| ( X == 0 && Y == 6) || (X == 1 && Y == 6) || (X == 1 && Y == 7)
	|| ( X == 6 && Y == 6) || (X == 7 && Y == 6) || (X == 6 && Y == 7))
	score /= 3;
    delete testBoard;
    return score;
}

int Board::minimaxGain(Move *m, Board *branch, Side side,
	 Side original, int ply)
{
    int topscore = -64;
    Side other = (side == BLACK) ? WHITE : BLACK;
    branch->doMove(m, side);
    if(ply <= 1)
    {
	 Side otherOrig = (original == BLACK) ? WHITE : BLACK;
	 int score = branch->count(original) - branch->count(otherOrig);
/*	 int X = m->getX();
	 int Y = m->getY();
	    //corners and around corners
	 if((X == 0 && Y == 0) || (X == 7 && Y == 0) || (X == 0 && Y == 7) ||
		(X == 7 && Y == 7))
		 score -= 40;
	 else if((X == 1 && Y == 1) || (X == 0 && Y == 1) || (X == 1 && Y == 0)
	|| ( X == 6 && Y == 0) || (X == 7 && Y == 1) || (X == 6 && Y == 1)
	|| ( X == 0 && Y == 6) || (X == 1 && Y == 6) || (X == 1 && Y == 7)
	|| ( X == 6 && Y == 6) || (X == 7 && Y == 6) || (X == 6 && Y == 7))
		score += 40;
        if(score > 64)
		score = 64;
	if(score < -64)
		score = -64;*/
	return score;
    }
    branch->getValidMoves(other);
    if(branch->validMoves.count() < 1) {
	return branch->minimaxGain(NULL, branch->copy(), other,
			original, ply - 1);
    }
    for(int k = 0; k < 64; k++)    {
	    if(branch->validMoves.test(k))
	    {
		int testscore = branch->minimaxGain(parseMove(k),
			 branch->copy(), other, original, ply - 1);
		if(testscore > topscore)
			topscore = testscore;
	    }
    }
    delete branch;
    return topscore;
}

Move *Board::minimaxMove(Side side, int ply) {
    int targets = getValidMoves(side);
    if(targets == 0)
	return NULL;
    Move *best = NULL;
    int topscore = -64;
    for(int k = 0; k < 64; k++)    {
	if(validMoves.test(k))	{
		int zzz = minimaxGain(parseMove(k), copy(), side,
					side, ply);
		if(zzz  > topscore)
		{
			topscore = zzz;
			best = parseMove(k);		
		}
		
	}
    }
    return best;

}

/*
 * For the AI consistent enough to beat SimplePlayer. Uses the 
 * heuristic described above in the set, which does it based
 * on pure new score and corner modifiers. 
 */
Move * Board::basicHeuristicMove(Side side) {
    int targets = getValidMoves(side);
    if(targets == 0)
	return NULL;
    Move *best = NULL;
    int topscore = 0;
    for(int k = 0; k < 64; k++)    {
	if(validMoves.test(k))	{
		if(scoreMove1(parseMove(k), side) > topscore)
		{
			topscore = scoreMove1(parseMove(k), side);
			best = parseMove(k);		
		}
		
	}
    }
    return best;
}

/*
 * Randomly picks a valid move, for the working implementation
 */
Move* Board::getRandomMove(Side side) {
    int targets = getValidMoves(side);
    int pick = rand() % targets;
    int current = 0;
    for(int k = 0; k < 64; k++)    {
	if(validMoves.test(k))	{
		if(current == pick)
		{
			return parseMove(k);
		}
		else
			current++;
	}
    }
    return NULL;
/*    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Move *move = new Move(i, j);
            if (checkMove(move, side)) return move;
        }
    }
    return NULL;*/
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[]) {
    taken.reset();
    black.reset();
    for (int i = 0; i < 64; i++) {
        if (data[i] == 'b') {
            taken.set(i);
            black.set(i);
        } if (data[i] == 'w') {
            taken.set(i);
        }
    }
}
